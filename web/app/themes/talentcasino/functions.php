<?php

	// enqueue child theme stylesheet
	add_action( 'wp_enqueue_scripts', 'vh_childtheme_enqueue_styles', 1000);
	function vh_childtheme_enqueue_styles() {
	    wp_enqueue_style( 'child-theme', get_stylesheet_directory_uri() .'/custom.css');
	}


	// make child theme use custom translation files
	function child_theme_slug_setup() {
	    load_child_theme_textdomain( 'vegashero-theme', get_stylesheet_directory() . '/languages' );
	}
	add_action( 'after_setup_theme', 'child_theme_slug_setup' );

?>
