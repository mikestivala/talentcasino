<?php
/*
Template Name: Casino page template
*/
?>

<?php get_header();?>

<?php if (of_get_option('title_banner_strip_checkbox', '1')) { ?>

<div class="game-post-banner">
	<h2><?php if (get_field('field_title_banner_strip')) { echo get_field('field_title_banner_strip'); } else { the_title(); } ?></h2>
	<div class="image" style="background: url('<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $image_url; ?>') no-repeat; background-size:cover;">
	</div>
</div>

<?php } ?>

<div id="content">
<div id="inner-content" class="row">

	<div class="large-12 medium-12">
		<div class="row collapse">
			<div id="main" class="large-8 medium-12 columns">

				<h1><?php the_title(); ?></h1>

				<?php

				if (of_get_option('casino_posts_ontop')) { get_template_part( 'parts/loop', 'casinos' ); }
				wp_reset_query();


					if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile; else :
						get_template_part( 'parts/content', 'missing' );
					endif;					

					$thecontent = get_the_content();
					if(!empty($thecontent)) {
						echo "<br>";
					}

					if (of_get_option('casino_posts_ontop') == '0') { get_template_part( 'parts/loop', 'casinos' ); } 

				?>

			</div>
			<div class="sidebar-wrapper-casino large-4 medium-12 divider columns">
				<?php get_sidebar(); ?>
			</div>

		</div>
	</div>

</div>
</div>
<?php get_footer(); ?>
