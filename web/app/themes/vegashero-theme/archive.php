<?php get_header(); ?>

<?php if (of_get_option('title_banner_strip_checkbox', '1')) { ?>

<div class="game-post-banner">
	<h2><?php if ( is_archive() ) { echo the_archive_title(); } else { echo of_get_option( 'archive_title_text', 'All News'); } ?></h2>
	<div class="image" style="background: url('<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $image_url; ?>') no-repeat; background-size:cover;">
	</div>
</div>

<?php } ?>

	<div id="content">

		<div id="inner-content" class="row">
				
		    <main id="main" class="large-8 medium-12 small-12 columns" role="main">

		    	<header class="large-12 medium-12 columns">
					<h2><?php the_archive_title();?></h2>
					<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>

				</header>

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'archive' ); ?>

				<?php endwhile; ?>

					<?php joints_page_navi(); ?>

				<?php else : ?>

					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>

		    </main> <!-- end #main -->
				<div class="large-4 medium-12 columns divider sidebar-wrapper-news">

					<?php get_sidebar( 'archive' ); ?>

				</div>
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
