<?php

	$shortcode = of_get_option( 'top_games_menu_shortcode' );

	if (of_get_option( 'top_games_menu_shortcode')) {
		echo do_shortcode($shortcode);
	} else { 
		echo _e( 'No games to display', 'vegashero-theme' );
	}

?>
