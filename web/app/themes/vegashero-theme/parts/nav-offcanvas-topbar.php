<!-- By default, this menu will use off-canvas for small
and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
	<div class="row">
		<div class="top-bar-left float-left logo-wrapper">
			<?php if (of_get_option( 'logo_icon')) { ?>
				<img src="<?php echo of_get_option('logo_icon'); ?>" class="logo-icon" alt="<?php bloginfo('name'); ?>" />
			<?php } ?>
			<?php
				if (of_get_option('logo_image')) {
					?>
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo of_get_option('logo_image'); ?>" class="logo-image" alt="<?php bloginfo('name'); ?>" />
					</a>
			<?php
				} else {
			?>
			<div class="heading-logo">
				<h1 class="logo">
					<a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
				</h1>
				<h2 class="site-description">
					<?php echo bloginfo('description'); ?>
				</h2>
			</div>
			<?php
				}
			 ?>
		</div>
		
			<div class="top-bar-right float-right">
				<button class="menu-mobile show-for-small-only" id="mobile-menu" type="button">Menu</button>
				<?php if (of_get_option('top_games_menu_checkbox','1')) { ?>
				<a href="#" type="button" id="nav-toggle" data-toggle="games" class="games-menu float-right"><span></span></a>
				<?php } ?>
			</div>
		<?php joints_top_nav(); ?>
	</div>
</div>
