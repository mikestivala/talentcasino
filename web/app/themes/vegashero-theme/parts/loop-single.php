<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="articleBody">

	<?php if ( is_singular('post') ) { ?>
		<div class="singlepost-thumb">
			<?php the_post_thumbnail('full'); ?>
		</div>
	<?php } ?>

	<?php
	if ( is_singular('casino_type') ) {
		if (has_post_thumbnail()) { ?>
			<div class="casino-thumb row">
				<div class="large-2 medium-2 small-12 columns">
					<a href="<?php echo the_field('affiliate-url'); ?>" title="<?php the_title_attribute(); ?>" <?php if (of_get_option('casino_link_target_checkbox', '1')) { ?>target="_blank"<?php } ?>><?php echo the_post_thumbnail('vh-casino-thumb'); ?></a>
				</div>
				<div class="large-10 medium-10 small-12 columns">
					<h3><a href="<?php echo the_field('affiliate-url'); ?>" title="<?php the_title_attribute(); ?>" <?php if (of_get_option('casino_link_target_checkbox', '1')) { ?>target="_blank"<?php } ?>><?php the_title(); ?><i><img alt="link icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/link-icon.svg"></i></a></h3>
					<h4><?php the_field('field_bonus_offer'); ?></h4>
				</div>
			</div>
			<?php
			}
		}
		?>
		<?php the_content(); ?>
		<?php
			if (is_singular('vegashero_games' || 'game_type')) {
				if (of_get_option( 'bonus_table')) {
					if (of_get_option( 'bonus_table_title')) { ?>
						<h2><?php echo of_get_option( 'bonus_table_title'); ?></h2>
						<?php }
						$bonus_table = of_get_option( 'bonus_table');
						echo do_shortcode($bonus_table);
			 			}
					}
				?>
	</section> <!-- end article section -->

			<footer class="article-footer">
				<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'vegashero-theme' ) . '</span> ', ', ', ''); ?></p>	</footer> <!-- end article footer -->

				<?php comments_template(); ?>

			</article> <!-- end article -->
