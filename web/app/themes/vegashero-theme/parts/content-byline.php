<?php if (is_singular('vegashero_games')) {  // don't display byline category and author on games ?>

	<h4 class="byline"><?php the_time('F j, Y') ?></h4>

<?php } else { ?>

	<h4 class="byline">
		<?php the_category(', '); ?>
		<?php the_time('F j, Y') ?> <?=_e( 'by', 'vegashero-theme' );?> <?php the_author_posts_link(); ?>
	</h4>

<?php } ?>
