<?php
$posts_on_homepage = of_get_option( 'home_posts', '5');
$args = array( 'posts_per_page', 'category', 'posts_per_page' => $posts_on_homepage, 'paged' => $paged );
$lastposts = get_posts($args);
$num_comments = get_comments_number();
$count = 0;
$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
$get_category = get_category_link(of_get_option('news_select_categories'));
$get_category_name = get_the_category_by_id(of_get_option('news_select_categories','1'));


// if ( is_wp_error( $get_category_name ) ) {
//     $error_string = $get_category_name->get_error_message();
//     echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
// }


foreach ( $lastposts as $post ) :
  setup_postdata( $post );

  if ($count == 0) {
    if ( has_post_thumbnail() ) { ?>
        <div class="large-12 post-banner" style="background: url('<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $image_url; ?>') no-repeat; background-size:cover;">
          <div class="large-12 overlay">
            <span class="category-label"><?=the_category(); ?></span>
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <h5><?=get_the_date('j F Y'); ?></h5>
          </div>
        </div>
      <?php }
    } else { ?>
        <div class="row large-12 list-post collapse">
          <div class="large-5 small-12 medium-5 columns">
              <?php
                if ( has_post_thumbnail() ) { ?>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('vh-archives-thumb'); ?></a>
                <?php } else { ?>
              <div class="no-image-radius archive">
                <span><?php $title = get_the_title(); echo $title[0];?></span>
              </div>
              <?php } ?>
          </div>
          <div class="large-7 small-12 medium-7 columns">
            <h5 class="post-details"><?=get_the_date('j F Y'); ?></h5>
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p>
              <?php
              echo wp_trim_words(strip_shortcodes(get_the_content()), 20, '...');
              ?>
            </p>
            <a href="<?php the_permalink() ?>" class="read-more" rel="bookmark" title="<?php the_title_attribute(); ?>"><?=_e( 'read more', 'vegashero-theme' );?></a>
          </div>
        </div>
        <?php }

        $count++;

      endforeach;

      ?>

      <div class="row">
        <div class="large-12 columns">
          <?php if ( of_get_option( 'display_all_posts_button_checkbox') == "1" ) {
            if (of_get_option( 'display_all_posts_button_text')) {
              ?>
              <a href="<?php echo $get_category ?>" class="button"><?php echo of_get_option( 'display_all_posts_button_text', 'More news');?></a>
              <?php }
            } else { ?>
              <a href="<?php echo get_permalink(get_option('page_for_posts' )); ?>" class="button"><?php echo 'All ' . $get_category_name; ?></a>
              <?php } ?>
            </div>
          </div>
          <?php
          wp_reset_postdata(); ?>

          <?php if ( of_get_option( 'banner_area' )) { ?>
            <a href="<?php echo of_get_option ( 'banner_area_link' ); ?>" ><img src="<?php echo of_get_option ( 'banner_area' ); ?>"></a>
            <?php } ?>
