<ul class="social-icons">
  <?php if ( of_get_option( 'facebook' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'facebook' ); ?>" class="mdi mdi-facebook-box" alt="Facebook"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'twitter' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'twitter' ); ?>" class="mdi mdi-twitter-box" alt="Twitter"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'google' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'google' ); ?>" class="mdi mdi-google-plus-box" alt="Google"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'pinterest' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'pinterest' ); ?>" class="mdi mdi-pinterest-box" alt="Pinterest"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'instagram' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'instagram' ); ?>" class="mdi mdi-instagram" alt="Instagram"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'linkedin' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'linkedin' ); ?>" class="mdi mdi-linkedin-box" alt="Linkedin"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'tumblr' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'tumblr' ); ?>" class="mdi mdi-tumblr" alt="Tumblr"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'rss' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'rss' ); ?>" class="mdi mdi-rss-box" alt="RSS"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'reddit' ) ) { ?>
  <li class="reddit-icon"><a href="<?php echo of_get_option( 'reddit' ); ?>" class="mdi mdi-reddit" alt="Reddit"></a></li>
  <?php } ?>
  <?php if ( of_get_option( 'youtube' ) ) { ?>
  <li><a href="<?php echo of_get_option( 'youtube' ); ?>" class="mdi mdi-youtube-play" alt="Youtube"></a></li>
  <?php } ?>
</ul>
