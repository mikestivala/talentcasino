<?php
$games_on_homepage = of_get_option( 'game_posts');
$args = array('post_type' => 'game_type');
$myposts = get_posts($args);

if(empty($myposts)){
	echo "You have no games";
} else {
?>
<ul class="vh-row-sm clearfix">
<?php
foreach ( $myposts as $post ) : setup_postdata( $post );
?>
	<li class="vh-item">
		<a href="<?php the_permalink(); ?>">
			<div class="vh-overlay">
				<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail('full');
					}	?>
				<span class="play-now"><?php echo _e( 'Play now', 'vegashero-theme' );?></span>
			</div>
			<div class="vh-game-title"><?php the_title(); ?></div>
		</a>
	</li>
<?php endforeach;
wp_reset_postdata();
?>
</ul>
<?php if ( of_get_option( 'display_all_games_button_checkbox', '1')) {
	if (of_get_option( 'display_all_games_button_text')) { ?>
		<a href="<?php the_permalink() ?>" class="button"><?php echo of_get_option( 'display_all_games_button_text', 'More games'); ?></a>
<?php }
	}
}?>
