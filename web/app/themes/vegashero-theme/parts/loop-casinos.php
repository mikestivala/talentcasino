<?php
$games_on_homepage = of_get_option( 'casino_posts', '6');
$args = array('post_type' => 'casino_type', 'posts_per_page' => $games_on_homepage, 'paged' => $paged);
query_posts($args);
?>

<div class="row casino-reviews-grid small-up-1 medium-up-2 large-up-3">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="column">
			<a href="<?php the_permalink(); ?>" class="card">
				<div class="vh-casino-title"><?php the_title(); ?></div>
					<div class="vh-casino-overlay">
							<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail('vh-casino-thumb');
								} else { ?>
									<div class="no-image-radius">
										<span><?php $title = get_the_title(); echo $title[0];?></span>
									</div>
							<?php
								}
							?>
					</div>
					<?php
						if (get_field('field_bonus_offer')) { ?>
						<div class="vh-bonus-text">
							<?php echo get_field('field_bonus_offer'); ?>
						</div>
						<?php
						}
					?>
					<div class="grid-casino-rating"><?php if (function_exists('wp_review_show_total')) wp_review_show_total(); ?></div>					
					<div <?php if (of_get_option('casino_posts_btn_override')) { ?>onclick="window.open('<?php echo get_field('field_affiliate_url');?>')"<?php } ?> class="button"><?php echo of_get_option( 'casino_posts_btn_text', 'Get Bonus'); ?></div>

			</a>
			
		</div>


		<?php endwhile; ?>

		<?php joints_page_navi(); ?>

	<?php else : ?>

		<?php echo _e( 'No casinos found', 'vegashero-theme' ); ?>

	<?php endif; 

	//wp_reset_query();

	?>

</div>
