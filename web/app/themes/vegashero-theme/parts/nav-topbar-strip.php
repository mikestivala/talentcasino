<div class="row">
	<div class="top-bar" id="main-menu">
		<div class="top-bar-left">
			<?php joints_top_nav_strip(); ?>
		</div>
		<?php if (of_get_option('facebook') || of_get_option('twitter') || of_get_option('google') || of_get_option('pinterest') || of_get_option('instagram') || of_get_option('linkedin') || of_get_option('tumblr') || of_get_option('rss') || of_get_option('reddit') || of_get_option('youtube')) { ?>
		<div class="top-bar-right">
			<?php get_template_part('parts/social-icons'); ?>
		</div>
		<?php } ?>
	</div>
</div>
