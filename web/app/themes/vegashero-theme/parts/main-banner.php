<?php
$banner_image = of_get_option( 'homepage_banner' );
$layerslider_banner = of_get_option( 'layerslider' );

if (of_get_option( 'layerslider')) {
	echo do_shortcode($layerslider_banner);
} else { ?>
	<div class="large-banner large-12 medium-12 clearfix" style="background-image:url(<?php echo $banner_image; ?>);">
		<div class="row">
			<div class="large-12 medium-12 columns relative">
				<?php if (of_get_option( 'banner_large_title')) { ?>
					<div class="banner-overlay-box">
						<h2><?php echo of_get_option( 'banner_large_title'); ?></h2>
						<h3><?php echo of_get_option( 'banner_description_title'); ?></h3>
						<?php if(of_get_option( 'banner_button_title')){ ?>
							<a href="<?php echo of_get_option( 'banner_button_link'); ?>" class="button"><?php echo of_get_option( 'banner_button_title', 'Claim bonus'); ?></a>
						<?php	} ?>
					</div>
				<?php	} ?>
			</div>
		</div>
	</div>
<?php } ?>
