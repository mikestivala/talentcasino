<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">


	<div class="row large-12 list-post collapse">
		<div class="large-4 medium-4 small-12 columns">
				<?php

				global $post;
				//check if it's VegasHero taxonomy archive
				if (( is_tax() ) && ( $post->post_type == 'vegashero_games' )) {
					//echo "hellothere";
					$game_thumb = get_post_meta( get_the_ID(), 'game_img', true );
					if ($game_thumb == '') { ?>
						<div class="no-image-radius archive">
							<span><?php $title = get_the_title(); echo $title[0];?></span>
						</div>
					<?php } else { ?>
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $game_thumb; ?>" alt="<?php the_title(); ?>" />
						</a>
					<?php }

				} else {
					if ( has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('vh-archives-thumb'); ?>
						</a>
				<?php
				 	} else { ?>
				<div class="no-image-radius archive">
					<span><?php $title = get_the_title(); echo $title[0];?></span>
				</div>
				<?php } 
				}
				?>
		</div>

		<div class="large-8 medium-8 small-12 columns">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
			
			<p class="archives-excerpt">
				<?php
					echo wp_trim_words(strip_shortcodes(get_the_content(), 30, '...'));
				?>
			</p>
			<a href="<?php the_permalink() ?>" class="read-more" rel="bookmark" title="<?php the_title_attribute(); ?>"><?=_e( 'Read more', 'vegashero-theme' );?></a>
			<h4 class="byline tags-list"><?php the_tags('<span class="tags-title">' . __('Tags:', 'vegashero-theme') . '</span> ', ', ', ''); ?></h4>
			
		</div>
	</div>
</article> <!-- end article -->
