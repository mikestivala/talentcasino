<?php
/*
Template Name: Homepage Template
*/
$lobby_item_size = of_get_option( 'lobby_home_sizes', '50');

$homepage_layout = of_get_option( 'homepage_size', '70');

if ($homepage_layout == "70") {
	$first_column = "7";
	$second_column = "5";
} else {
	$first_column = "6";
	$second_column = "6";
}
?>

<?php get_header();?>

<?php
if (of_get_option('enable_banner_slider','1')) {
	if ( is_front_page() && is_home() ) {
			get_template_part( 'parts/main', 'banner' );
		} elseif ( is_front_page()) {
			get_template_part( 'parts/main', 'banner' );
		} elseif ( is_home()){

		} else {
	}
}
?>

<div class="large-12 medium-12">
	<div class="row">

		<div class="large-<?=$first_column?> medium-12 columns">
			<div class="large-12 home-top-sidebar">
				<?php
					if ( is_active_sidebar( 'home-sidebar-top' ) ) :
						dynamic_sidebar( 'home-sidebar-top' );
					endif;
				?>
			</div>
			<div class="large-12 home-posts">
				<?php if (of_get_option( 'homepage_posts_title_text')) { ?>
	          <h2><?php echo of_get_option( 'homepage_posts_title_text', 'Latest news'); ?></h2>
	      <?php } else { ?>
					<h2><?=_e( 'Latest News', 'vegashero-theme' );?></h2>
				<?php } ?>
				<?php get_template_part( 'parts/loop', 'list' ); ?>
			</div>
			<div class="large-12 home-sidebar-1">
				<?php
					if ( is_active_sidebar( 'home-sidebar-1' ) ) :
						dynamic_sidebar( 'home-sidebar-1' );
					endif;
				?>
			</div>
		</div>
		<div class="large-<?=$second_column?> medium-12 columns">
			<div class="row">
				<div class="large-12 medium-12 columns home-sidebar-2">
					<?php if ( is_active_sidebar( 'home-sidebar-2' ) ) :
									dynamic_sidebar( 'home-sidebar-2' );
								endif; ?>
					</div>
				</div>
			<div class="row">
				<div class="large-6 medium-6 small-12 columns home-sidebar-3">
				<?php if ( is_active_sidebar( 'home-sidebar-3' ) ) :
								dynamic_sidebar( 'home-sidebar-3' );
							endif; ?>
				</div>
				<div class="large-6 medium-6 small-12 columns home-sidebar-4">
					<?php if ( is_active_sidebar( 'home-sidebar-4' ) ) :
									dynamic_sidebar( 'home-sidebar-4' );
								endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
