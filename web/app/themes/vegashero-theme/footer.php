					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
								<div class="large-3 medium-6 small-12 columns">
									<?php if ( is_active_sidebar( 'footer1' ) ) :
										 			dynamic_sidebar( 'footer1' );
											 	else : ?>
									<div class="alert help">
										<p></p>
									</div>
									<?php endif; ?>
								</div>
								<div class="large-3 medium-6 small-12 columns">
									<?php if ( is_active_sidebar( 'footer2' ) ) :
										 			dynamic_sidebar( 'footer2' );
											 	else : ?>
									<div class="alert help">
										<p></p>
									</div>
									<?php endif; ?>
								</div>
								<div class="large-3 medium-6 small-12 columns">
									<?php if ( is_active_sidebar( 'footer3' ) ) :
										 			dynamic_sidebar( 'footer3' );
											 	else : ?>
									<div class="alert help">
										<p></p>
									</div>
									<?php endif; ?>
								</div>
								<div class="large-3 medium-6 small-12  columns">
									<?php if ( is_active_sidebar( 'footer4' ) ) :
										 			dynamic_sidebar( 'footer4' );
											 	else : ?>
									<div class="alert help">
										<p></p>
									</div>
									<?php endif; ?>
								</div>
						</div> <!-- end #inner-footer -->
						<div class="large-12 medium-12 columns footer-strip">
							<div class="row">
								<div class="logo-footer">
									<?php bloginfo('name'); ?>
									<span><?php bloginfo('description'); ?></span>
								</div>
								<nav role="navigation">
									<?php joints_footer_links(); ?>
									<?php if (of_get_option('facebook') || of_get_option('twitter') || of_get_option('google') || of_get_option('pinterest') || of_get_option('instagram') || of_get_option('linkedin') || of_get_option('tumblr') || of_get_option('rss') || of_get_option('reddit') || of_get_option('youtube')) { ?>
										<?php get_template_part('parts/social-icons'); ?>
									<?php } ?>
								</nav>
							</div>
						</div>
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>

</html> <!-- end page -->
