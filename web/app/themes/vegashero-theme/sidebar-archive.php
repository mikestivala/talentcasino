<?php if ( is_active_sidebar( 'archive-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'archive-sidebar' ); ?>
<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->

<div class="alert help nowidget">
	<p><?php _e( 'No widgets added', 'vegashero-theme' );  ?></p>
</div>

<?php endif; ?>
