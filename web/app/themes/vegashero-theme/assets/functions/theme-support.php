<?php

// Adding WP Functions & Theme Support
function joints_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	// Default thumbnail size
	set_post_thumbnail_size(376, 250, true);

	add_post_type_support( 'vegashero_games', 'excerpt' );
	add_post_type_support( 'casino_type', 'excerpt' );

	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );

	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );

	// Add HTML5 Support
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	add_filter( 'widget_text', 'do_shortcode');

	// Adding post format support
	/* add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

} /* end theme support */

function custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * WP-Review defaults - partially working.
 */

// Set location for selected or all reviews
function mts_new_review_location($position, $id) {
	//$post_types = array('casino_type');
	$post_type = get_post_type( $id );
	if ($post_type == 'casino_type') {
	  $position = 'top';
	  return $position;
	}
	if ($post_type == 'vegashero_games') {
	  $position = 'bottom';
	  return $position;
	}
}
add_filter( 'wp_review_location', 'mts_new_review_location', 10, 2 );

// Remove banner from options page
add_filter( 'wp_review_remove_branding', '__return_true' );


// Hide fields in "item" meta box
add_action('admin_head', 'hide_review_item_fields_css');
function hide_review_item_fields_css() {
  echo '<style>
  	/*hide_review_item_fields_css*/
    #wp_review_custom_location, label[for=wp_review_custom_location],
    #wp_review_custom_colors, label[for=wp_review_custom_colors],
    #wp_review_type option[value=point], #wp_review_type option[value=percentage],
    #rating_type option[value=point], #rating_type option[value=percentage] {
    	display:none;
    } 
  </style>';
}


// Hide fields in "item" meta box - NOT WORKING
function mts_hide_item_metabox_fields($fields) {
  unset($fields['location']);
  return $fields;
}
add_filter( 'wp_review_metabox_item_fields', 'mts_hide_item_metabox_fields' );


// Hide selected review types in metabox dropdown - NOT WORKING
function mts_hide_review_types($types) {
  unset($types['point'], $types['percentage']); // remove types
  //$types['star'] = __('Enable Reviews'); // Change label
  return $types;
}
add_filter( 'wp_review_metabox_types', 'mts_hide_review_types' );








