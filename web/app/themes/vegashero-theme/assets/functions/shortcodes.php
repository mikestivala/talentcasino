<?php
function buffer_include_html($path, $atts, $content){
  ob_start();
  echo $path;
  $result = ob_get_contents();
  ob_end_clean();
  return $result;
}

if( ! class_exists('acf') ) {
function remove_menus(){
   remove_menu_page( 'edit.php?post_type=acf' ); //Remove Post Type ACF from admin menus
}
add_action( 'admin_init', 'remove_menus' );
}

function review($atts,$content = null){
  return buffer_include_html('<ul class="rating">' . do_shortcode($content) . '</ul>', $atts, $content);
}
add_shortcode( 'review', 'review');

function rating($atts, $content = null) {
  $atts = shortcode_atts(array(
    'display' => ''
  ), $atts);
  return buffer_include_html('<li class="' . $atts['display'] . '"></li>', $atts, $content);
}
add_shortcode( 'rating', 'rating');

/*--------------- CUSTOM FIELDS --------------------- */
include_once get_template_directory() . '/advanced-custom-fields/acf.php';
// define( 'ACF_LITE', true );
if(function_exists("register_field_group")){
  register_field_group(array (
    'id' => 'acf_game-page-options',
    'title' => 'Game page options',
    'fields' => array (
      array (
      'key' => 'field_play_for_real_url',
      'label' => 'Visit Casino button',
      'name' => 'play-real-button',
      'type' => 'text',
      'instructions' => 'Add in your affiliate link (optional), this will be your "Visit Casino" button',
      'default_value' => 'Visit Casino',
      'placeholder' => 'Enter your full url here',
      'prepend' => 'Enter URL',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
    ),
    array (
      'key' => 'field_iframe_url',
      'label' => 'Add iFrame to post',
      'name' => 'iframe-code',
      'type' => 'text',
      'instructions' => 'Add in your iframe code to your game page',
      'default_value' => '',
      'placeholder' => 'Enter the full iframe code here eg: <iframe src="">',
      'prepend' => 'Enter code',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
    ),
  ),
  'location' => array (
    array (
      array (
      'param' => 'post_type',
      'operator' => '==',
      'value' => 'game_type',
      'order_no' => 0,
      'group_no' => 0,
      ),
    ),
  ),
  'options' => array (
    'position' => 'normal',
    'layout' => 'no_box',
    'hide_on_screen' => array (
    0 => 'excerpt',
    1 => 'custom_fields',
    2 => 'discussion',
    3 => 'comments',
    4 => 'revisions',
    5 => 'author',
    6 => 'format',
    7 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
  ));
}
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_turn-off-excerpt',
		'title' => 'Turn off excerpt',
		'fields' => array (
			array (
				'key' => 'field_58833a5854965',
				'label' => 'Excerpt Checkbox',
				'name' => 'excerpt_checkbox',
				'type' => 'checkbox',
				'instructions' => 'Turn off sidebar excerpt',
				'choices' => array (
					'off' => 'Off',
				),
				'default_value' => '',
				'layout' => 'horizontal',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'game_type',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group")){
  register_field_group(array (
    'id' => 'acf_casino-page-options',
    'title' => 'Casino page options',
    'fields' => array (
      array (
        'key' => 'field_affiliate_url',
        'label' => 'Affiliate URL',
        'name' => 'affiliate-url',
        'type' => 'text',
        'instructions' => 'Add in your affiliate link (optional), this will be your "Visit Casino" button',
        'default_value' => '',
        'placeholder' => 'Enter your full url here',
        'prepend' => 'Enter URL',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    array (
      'key' => 'field_bonus_offer',
      'label' => 'Bonus Offered',
      'name' => 'bonus-offer',
      'type' => 'text',
      'instructions' => 'Add in the bonus offer for the Casino',
      'default_value' => '',
      'placeholder' => 'Enter the bonus here',
      'prepend' => 'Enter bonus',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
    ),
    array (
      'key' => 'field_title_banner_strip',
      'label' => 'Alternate H2 Title in Banner Strip',
      'name' => 'title-banner-strip',
      'type' => 'text',
      'instructions' => 'Customize the top strip text (by default it will be the same as the post title)',
      'default_value' => '',
      'placeholder' => 'Enter alternate title here',
      'prepend' => 'Enter title',
      'append' => '',
      'formatting' => 'html',
      'maxlength' => '',
    ),
  ),
  'location' => array (
    array (
      array (
      'param' => 'post_type',
      'operator' => '==',
      'value' => 'casino_type',
      'order_no' => 0,
      'group_no' => 0,
      ),
    ),
  ),
  'options' => array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => array (
    //0 => 'excerpt',
    1 => 'custom_fields',
    //2 => 'discussion',
    //3 => 'comments',
    //4 => 'revisions',
    //5 => 'author',
    6 => 'format',
    7 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
  ));
}

if(function_exists("register_field_group")){
  register_field_group(array (
    'id' => 'acf_vhgame-page-options',
    'title' => 'Game page options',
    'fields' => array (
        array (
        'key' => 'field_title_banner_strip',
        'label' => 'Alternate H2 Title in Banner Strip',
        'name' => 'title-banner-strip',
        'type' => 'text',
        'instructions' => 'Customize the top strip text (by default it will be the same as the post title)',
        'default_value' => '',
        'placeholder' => 'Enter alternate title here',
        'prepend' => 'Enter title',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
  'location' => array (
    array (
      array (
      'param' => 'post_type',
      'operator' => '==',
      'value' => 'vegashero_games',
      'order_no' => 0,
      'group_no' => 0,
      ),
    ),
  ),
  'options' => array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => array (
    //0 => 'excerpt',
    //1 => 'custom_fields',
    //2 => 'discussion',
    //3 => 'comments',
    //4 => 'revisions',
    //5 => 'author',
    6 => 'format',
    7 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
  ));
}


if(function_exists("register_field_group")){
  register_field_group(array (
    'id' => 'acf_default-page-options',
    'title' => 'Page options',
    'fields' => array (
        array (
        'key' => 'field_title_banner_strip',
        'label' => 'Alternate H2 Title in Banner Strip',
        'name' => 'title-banner-strip',
        'type' => 'text',
        'instructions' => 'Customize the top strip text (by default it will be the same as the post title)',
        'default_value' => '',
        'placeholder' => 'Enter alternate title here',
        'prepend' => 'Enter title',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
  'location' => array (
    array (
      array (
      'param' => 'post_type',
      'operator' => '==',
      'value' => 'page',
      'order_no' => 0,
      'group_no' => 0,
      ),
    ),
  ),
  'options' => array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => array (
    //0 => 'excerpt',
    1 => 'custom_fields',
    2 => 'discussion',
    3 => 'comments',
    //4 => 'revisions',
    //5 => 'author',
    6 => 'format',
    7 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
  ));
}


if(function_exists("register_field_group")){
  register_field_group(array (
    'id' => 'acf_post-page-options',
    'title' => 'Post options',
    'fields' => array (
        array (
        'key' => 'field_title_banner_strip',
        'label' => 'Alternate H2 Title in Banner Strip',
        'name' => 'title-banner-strip',
        'type' => 'text',
        'instructions' => 'Customize the top strip text (by default it will be the same as the post title)',
        'default_value' => '',
        'placeholder' => 'Enter alternate title here',
        'prepend' => 'Enter title',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
  'location' => array (
    array (
      array (
      'param' => 'post_type',
      'operator' => '==',
      'value' => 'post',
      'order_no' => 0,
      'group_no' => 0,
      ),
    ),
  ),
  'options' => array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => array (
    //0 => 'excerpt',
    1 => 'custom_fields',
    //2 => 'discussion',
    //3 => 'comments',
    //4 => 'revisions',
    //5 => 'author',
    6 => 'format',
    7 => 'send-trackbacks',
    ),
  ),
  'menu_order' => 0,
  ));
}


// Casino cards grid shortcode
function vh_casino_grid( $atts ) {
  $casino_posts_count = of_get_option( 'casino_posts', '6');
  //$casino_ratings = get_post_meta( get_the_ID(), 'wp_review_total', true );
  $atts = shortcode_atts( array(
    'postcount' => $casino_posts_count,
    'category' => '',
    //'pagination' => '',
    'orderby' => 'date',
    'order' => 'DESC'
  ), $atts, 'vh-casino-grid' );

  ob_start();

    $args1 = array(
      'post_type' => 'casino_type',
      'posts_per_page' => $atts['postcount'],
      'orderby' => $atts['orderby'],
      'order' => $atts['order'],
      //'meta_key'  => 'wp_review_total',
    );

    $args2 = array(
      'post_type' => 'casino_type',
      'posts_per_page' => $atts['postcount'],
      'orderby' => $atts['orderby'],
      'order' => $atts['order'],
      'tax_query' => array(
        array(
          'taxonomy' => 'casino_category',
          'field'    => 'slug',
          'terms'    => $atts['category'],
        ),
      ),
    );

    if ( $atts['category'] == '' ) {
      $query_args = $args1;
    } else {
      $query_args = $args2;
    }

    $query = new WP_Query( $query_args );

    if ( $query->have_posts() ) { ?>
        <div class="row casino-reviews-grid small-up-1 medium-up-2 large-up-3">

            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

              <div class="column">
                <a href="<?php the_permalink(); ?>" class="card">
                  <div class="vh-casino-title"><?php the_title(); ?></div>
                    <div class="vh-casino-overlay">
                        <?php
                          if ( has_post_thumbnail() ) {
                            the_post_thumbnail('vh-casino-thumb');
                          } else { ?>
                            <div class="no-image-radius">
                              <span><?php $title = get_the_title(); echo $title[0];?></span>
                            </div>
                        <?php
                          }
                        ?>
                    </div>
                    <?php
                      if (get_field('field_bonus_offer')) { ?>
                      <div class="vh-bonus-text">
                        <?php echo get_field('field_bonus_offer'); ?>
                      </div>
                      <?php
                      }
                    ?>
                    <div class="grid-casino-rating"><?php if (function_exists('wp_review_show_total')) wp_review_show_total(); ?></div>         
                    <div <?php if (of_get_option('casino_posts_btn_override')) { ?>onclick="window.open('<?php echo get_field('field_affiliate_url');?>')"<?php } ?> class="button"><?php echo of_get_option( 'casino_posts_btn_text', 'Get Bonus'); ?></div>
                </a>
              </div>
            <?php endwhile;
            wp_reset_postdata(); ?>

        </div>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
add_shortcode( 'vh-casino-grid', 'vh_casino_grid' );
