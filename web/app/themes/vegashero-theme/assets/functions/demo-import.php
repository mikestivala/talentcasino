<?php

function ocdi_import_files() {
    return array(
        array(
            'import_file_name'             => 'Demo Import VegasHero',
            //'categories'                   => array( 'Category 1', 'Category 2' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . '/assets/functions/ocdi/content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . '/assets/functions/ocdi/widgets.wie',
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/assets/functions/ocdi/customizer.dat',
            // 'local_import_redux'           => array(
            //     array(
            //         'file_path'   => trailingslashit( get_template_directory() ) . 'ocdi/redux.json',
            //         'option_name' => 'redux_option_name',
            //     ),
            // ),
            //'import_preview_image_url'     => '',
        )
    );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );


function ocdi_plugin_intro_text( $default_text ) {
    $default_text .= '<div class="ocdi__intro-text">Make sure all required plugins are activated before importing!</div>';

    return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'ocdi_plugin_intro_text' );

add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );


function ocdi_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $top_menu = get_term_by( 'name', 'Top Menu', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'main-nav' => $main_menu->term_id,
            'top-nav' => $top_menu->term_id,
            'footer-links' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Homepage' );
    $blog_page_id  = get_page_by_title( 'News' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );