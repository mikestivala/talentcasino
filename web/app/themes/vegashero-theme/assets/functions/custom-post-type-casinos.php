<?php

function custom_post_casinos() {
	register_post_type( 'casino_type', 
		array('labels' => array(
			// UI labels for Custom Post Type
			'name' => __('Casinos'),
			'singular_name' => __('Casino Review'),
			'all_items' => __('All Casino Reviews'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New Casino'),
			'edit' => __( 'Edit' ),
			'edit_item' => __('Edit Casino'),
			'new_item' => __('New Casino'),
			'view_item' => __('View Casino'),
			'search_items' => __('Search Casino'),
			'not_found' =>  __('No casinos found.'),
			'not_found_in_trash' => __('Nothing found in Trash'),
			'parent_item_colon' => ''
			), 
			// Other Custom Type Options
			'description' => __( 'This is the Casino custom post type' ), 
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-book', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => of_get_option('casino_cpt_slug', 'casinos'), 'with_front' => false), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'can_export'          => true,
			// the next one is important, it tells what's enabled in the post editor
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky'),
			// This is where we add taxonomies to our CPT
        	'taxonomies'          => array( 'casino_category' )
	 	) /* end of options */
	);
}

add_action( 'init', 'custom_post_casinos');


function custom_tax_casino_category() {
	register_taxonomy(
		'casino_category',
		'casino_type',
		array(

			'labels' => array(
				'name'              => __( 'Casino Categories' ),
				'singular_name'     => __( 'Casino Category' ),
				'search_items'      => __( 'Search Categories' ),
				'all_items'         => __( 'All Categories' ),
				'edit_item'         => __( 'Edit Category' ),
				'update_item'       => __( 'Update Category' ),
				'add_new_item'      => __( 'Add New Category' ),
				'new_item_name'     => __( 'New Category Name' ),
				'menu_name'         => __( 'Casino Categories' ),
			),
			
			'rewrite' => array( 'slug' => 'casino-category' ),
			'hierarchical' => false,

		)
	);
}

add_action( 'init', 'custom_tax_casino_category' );
