<?php

function vegashero_shortcode_table() {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$shortcode_file = WP_PLUGIN_DIR . '/vegashero/shortcodes.php';
	if (is_plugin_active( 'vegashero/vegashero.php' )) {
		$table_shortcode = new Vegashero_Shortcodes();
		add_filter( 'widget_text', 'do_shortcode', 11 );
		if ( is_active_sidebar( 'single_game_widget_area' ) ) :
			dynamic_sidebar( 'single_game_widget_area' );
		endif;
	}
}

function game_sidebar() {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$shortcode_file = WP_PLUGIN_DIR . '/tablepress/classes/class-render.php';
	if (is_plugin_active('tablepress/tablepress.php')) {
		if (class_exists('TablePress_Render')) {
			$table_shortcode = new TablePress_Render();
			$post_id = get_the_ID();
			//$table_data = $table_shortcode->run();
			if (is_singular('vegashero_games' || 'game_type')) {
				if (of_get_option('bonus_table')) {
					$bonus_table = of_get_option('bonus_table');
					$play_now_text = of_get_option('play_now_text', 'Play for real');
						echo "<div class='bonus-table-sidebar'>";
						?>
						<a class="large-cta button" id="play-mini-table"><?=$play_now_text?><i></i></a>
						<div id="mini-casino-table">
					<?php
						echo do_shortcode($bonus_table);
						echo "</div>";
						echo "</div>";
					}
				}
			}
	}
}


?>
