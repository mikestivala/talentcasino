<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/assets/js/what-input.min.js', array(), '', true );

    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.js', array( 'jquery' ), '6.3', true );

    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Material icons
    wp_enqueue_style( 'mdi', 'https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/1.9.33/css/materialdesignicons.min.css', array(), '', 'all' );
    //wp_enqueue_style( 'mdi', get_template_directory_uri() . '/node_modules/mdi/css/materialdesignicons.min.css', array(), '', 'all' );

    // Base fonts
    wp_enqueue_style('ptsans', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic');
    wp_enqueue_style('montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:200,300,400,700');
    wp_enqueue_style('opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
    wp_enqueue_style('ekmukta', 'https://fonts.googleapis.com/css?family=Ek+Mukta:400,200,300,500,600,700,800');

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
