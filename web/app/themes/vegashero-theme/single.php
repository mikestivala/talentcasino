<?php

get_header();
if(of_get_option('review_style_select') == 'light'){
	$review_style = "light";
} else {
	$review_style = "dark";
}
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/review-css-<?=$review_style?>.css" type="text/css" />

<?php if (of_get_option('title_banner_strip_checkbox', '1')) { ?>

<div class="game-post-banner">
	<h2><?php if (get_field('field_title_banner_strip')) { echo get_field('field_title_banner_strip'); } else { the_title(); } ?></h2>
	<div class="image" style="background: url('<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $image_url; ?>') no-repeat; background-size:cover;">
	</div>
</div>

<?php } ?>

<div id="content">
	<div id="inner-content" class="row">
		<main id="main" class="large-8 medium-12 columns" role="main">

			<?php
			if (have_posts()) : while (have_posts()) : the_post();
			get_template_part( 'parts/loop', 'single' );
		endwhile; else :
			get_template_part( 'parts/content', 'missing' );
		endif;
		?>

	</main> <!-- end #main -->

	<div id="sidebar1" class="sidebar-wrapper large-4 medium-12 columns divider game-post" role="complementary">
		<?php if ( is_singular( array( 'vegashero_games', 'casino_type' ) ) ) { ?>
			<div class="button-container <?php if (of_get_option('sticky_mobile_affiliate_link_checkbox')) { echo 'mobile-sticky'; } ?> <?php if (of_get_option('sticky_mobile_casino_link_checkbox')) { echo 'mobile-sticky-casino'; } ?>">
				<?php if (of_get_option('display_review_stars_checkbox')) {
					if (function_exists('wp_review_show_total')) wp_review_show_total();
				}
				if ( is_singular( array( 'vegashero_games' ) ) ) {
					if (of_get_option('global_link_checkbox')) {
						?>
						<a href="<?php
						if (of_get_option('global_affiliate_link')) {
							echo of_get_option('global_affiliate_link');
						}
						?>" class="button blue globalaff" target="_blank"><?php echo of_get_option('global_affiliate_link_text'); ?></a>
						<?php
					}
					if (!of_get_option('global_link_checkbox')) {
						include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
						if (is_plugin_active( 'vegashero/vegashero.php' ) && of_get_option('global_link_checkbox') == false) {
							if ( of_get_option( 'mini_table_checkbox', '1')) {
								game_sidebar();
							} else {
								//vegashero_shortcode_table();
							}
						} else {

						}
					}
				}
			}
			if ( is_singular( 'casino_type' ) ) {
				if (get_field('affiliate-url')) {
					$casino_title_button = of_get_option('casino_title_button', 'Visit Casino');
					?>
					<a href="<?php the_field('affiliate-url'); ?>" class="button blue casinocta" <?php if (of_get_option('casino_link_target_checkbox', '1')) { ?>target="_blank"<?php } ?>><?=$casino_title_button?></a>
					<?php
				}
			}
			if (has_excerpt()) {
				if ( is_singular( array( 'vegashero_games', 'casino_type' ) ) ) {
						?>
						<div class="widget">
							<?php if(of_get_option( 'game_excerpt_title' ) || of_get_option( 'casino_excerpt_title' )) {
								if ( is_singular( array( 'vegashero_games' ) ) ) {
									$details_title_text = of_get_option('game_excerpt_title', 'Game Details');
								}
								if ( is_singular( array( 'casino_type' ) ) ) {
									$details_title_text = of_get_option('casino_excerpt_title', 'Casino Details');
								}
								?>
								<h2 class="widgettitle"><?=$details_title_text?></h2>
								<?php } ?>
								<?php the_excerpt(); ?>
							</div>
							<?php
					}
				}
				get_sidebar();
				?>
			</div>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->
	<?php get_footer(); ?>
