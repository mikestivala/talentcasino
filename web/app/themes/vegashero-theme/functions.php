<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');
require_once(get_template_directory().'/assets/functions/menu-walkers.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');
require_once(get_template_directory().'/assets/functions/game-sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php');

// Plugin activation
require_once(get_template_directory().'/plugin-activation/class-tgm-plugin-activation.php');

// Plugin Function
require_once(get_template_directory().'/assets/functions/plugin-activation.php');

// Shortcodes
require_once(get_template_directory().'/assets/functions/shortcodes.php');

// Customizer
//require_once(get_template_directory().'/assets/functions/customizer.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php');

// Related post function - no need to rely on plugins
 require_once(get_template_directory().'/assets/functions/related-posts.php');

// Demo import
require_once(get_template_directory().'/assets/functions/demo-import.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/assets/functions/custom-post-type-casinos.php');

// This is for the options panel functions
require_once(get_template_directory().'/options-framework.php');

// Register your custom function to override some LayerSlider data
add_action('layerslider_ready', 'my_layerslider_overrides');
function my_layerslider_overrides() {

    // Disable auto-updates
    $GLOBALS['lsAutoUpdateBox'] = false;
}
/**
 * Extend Recent Posts Widget
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */

Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts {

	function widget($args, $instance) {

		extract( $args );

		$title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);

		if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
			$number = 10;

    query_posts(array('post_type' => 'post', 'post_status' => 'publish'));
    if( have_posts() ) :

			echo $before_widget;
			if( $title ) echo $before_title . $title . $after_title; ?>
			<ul>
				<?php while( have_posts() ) : the_post(); ?>
				<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
			</ul>

			<?php
			echo $after_widget;

		wp_reset_postdata();

		endif;
	}
}
function my_recent_widget_registration() {
  unregister_widget('WP_Widget_Recent_Posts');
  register_widget('My_Recent_Posts_Widget');
}
add_action('widgets_init', 'my_recent_widget_registration');
// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');


// Add social Open graph meta tags
function doctype_opengraph($output) {
    return $output . '
    xmlns:og="http://opengraphprotocol.org/schema/"
    xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'doctype_opengraph');

function fb_opengraph() {
    global $post;
 
    if(is_single()) {
        // if(has_post_thumbnail($post->ID)) {
        //     $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
        // }
        if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
		    $thumbnail_id = get_post_thumbnail_id($post->ID);
		    $thumbnail_object = get_post($thumbnail_id);
		    $img_src = $thumbnail_object->guid; 
		} else {
            //$img_src = get_stylesheet_directory_uri() . '/img/opengraph_image.jpg'; // add option for default image later
            $img_src = '';
        }

        if($excerpt = $post->post_excerpt) {
            $excerpt = strip_tags($post->post_excerpt);
            $excerpt = str_replace("", "'", $excerpt);
        } else {
            $excerpt = substr(strip_tags($post->post_content),0,200) . '...';
        }
        ?>
 
    <meta property="og:title" content="<?php echo the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo $img_src; ?>"/>
 
<?php
    } else {
        return;
    }
}
add_action('wp_head', 'fb_opengraph', 5);

// image size for archives pages and home news section
add_image_size( 'vh-archives-thumb', 376, 250, true);
// image size for casino post types
add_image_size( 'vh-casino-thumb', 300, 300, true);


/**
 * Load theme EDD updater functions.
 * Action is used so that child themes can easily disable.
 */

function vegashero_theme_updater() {
	require( get_template_directory() . '/updater/theme-updater.php' );
}
add_action( 'after_setup_theme', 'vegashero_theme_updater' );



/**
 * disables the games archives pages and gives priority to posts/pages with same URL slug
 * copy this function into your child themes function.php if you want to use the same slug for a lobby page as the game base url
 * Usage case: for example your game base url is youdomain.com/mygames/game-name/ and you want to have a lobby page like yourdomain.com/mygames/
 */
/*function custom_filter_vh_cpt( $args, $post_type ) {
    // If not vegashero_games CPT, bail.
    if ( 'vegashero_games' !== $post_type ) {
        return $args;
    }
    // Add additional vegashero_games CPT options.
    $vh_args = array(
        'has_archive' => false 
    );
    // Merge args together.
    return array_merge( $args, $vh_args );
}
add_filter( 'register_post_type_args', 'custom_filter_vh_cpt', 10, 2 );
 */



/**
 * add additional Casnio post categorization taxonomies
 * copy this function into your child themes function.php and customize the My Casino Type strings
 */
// register_taxonomy( 'My Casino Type', 'casino_type', array(
//             'label' => __( 'My Casino Types' ),
//             'rewrite' => array( 'slug' => 'my-casino-types' ),
//             'hierarchical' => false,
//         ) );
// register_taxonomy_for_object_type( 'My Casino Type', 'casino_type' );
