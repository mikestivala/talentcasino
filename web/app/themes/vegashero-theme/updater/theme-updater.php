<?php
/**
 * VegasHero Theme Updater
 *
 * @package VegasHero Theme
 */

// Includes the files needed for the theme updater
if ( !class_exists( 'EDD_Theme_Updater_Admin' ) ) {
	include( dirname( __FILE__ ) . '/theme-updater-admin.php' );
}

// Loads the updater classes
$updater = new EDD_Theme_Updater_Admin(

	// Config settings
	$config = array(
		'remote_api_url' => 'https://vegashero.co', // Site where EDD is hosted
		'item_name'      => 'VegasHero Theme', // Name of theme
		'theme_slug'     => 'vegashero-theme', // Theme slug
		'version'        => '1.7.0', // The current version of this theme
		'author'         => 'VegasHero', // The author of this theme
		'download_id'    => '132648', // Optional, used for generating a license renewal link
		'renew_url'      => '', // Optional, allows for a custom license renewal link
		'beta'           => false, // Optional, set to true to opt into beta versions
	),

	// Strings
	$strings = array(
		'theme-license'             => __( 'Theme License'),
		'enter-key'                 => __( 'Enter your theme license key to recieve automatic updates.'),
		'license-key'               => __( 'License Key'),
		'license-action'            => __( 'License Action'),
		'deactivate-license'        => __( 'Deactivate License'),
		'activate-license'          => __( 'Activate License'),
		'status-unknown'            => __( 'License status is unknown.'),
		'renew'                     => __( 'Renew?'),
		'unlimited'                 => __( 'unlimited'),
		'license-key-is-active'     => __( 'License key is active.'),
		'expires%s'                 => __( 'Expires %s.'),
		'expires-never'             => __( 'Lifetime License.'),
		'%1$s/%2$-sites'            => __( 'You have %1$s / %2$s sites activated.'),
		'license-key-expired-%s'    => __( 'License key expired %s.'),
		'license-key-expired'       => __( 'License key has expired.'),
		'license-keys-do-not-match' => __( 'License keys do not match.'),
		'license-is-inactive'       => __( 'License is inactive.'),
		'license-key-is-disabled'   => __( 'License key is disabled.'),
		'site-is-inactive'          => __( 'Site is inactive.'),
		'license-status-unknown'    => __( 'License status is unknown.'),
		'update-notice'             => __( "Updating this theme will lose any customizations you have made. 'Cancel' to stop, 'OK' to update."),
		'update-available'          => __('<strong>%1$s %2$s</strong> is available. <a href="%3$s" class="thickbox" title="%4s">Check out what\'s new</a> or <a href="%5$s"%6$s>update now</a>.'),
	)

);
