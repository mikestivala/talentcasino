<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'vegashero-theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Lobby thumb options
	$lobby_thumb_sizes = array(
		'50' => __( 'Half width'),
		'100' => __( 'Full width')
	);
	// Layout data
	$layout_array = array(
		'50' => __( '50%'),
		'70' => __( '70%')
	);

	// Style options
	$options_review_style = array(
		'dark' => __( 'Dark'),
		'light' => __( 'Light')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __( 'French Toast'),
		'two' => __( 'Pancake'),
		'three' => __( 'Omelette'),
		'four' => __( 'Crepe'),
		'five' => __( 'Waffle')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/options-framework/images/';

	$options = array();

	//************* GLOBAL SETTINGS *************//

	$options[] = array(
		'name' => __( 'Global Settings'),
		'type' => 'heading'
	);

	//************* END GLOBAL SETTINGS *************//

	// Logo image
	$options[] = array(
		'name' => __( 'Logo image'),
		'desc' => __( 'Upload logo image'),
		'id' => 'logo_image',
		'type' => 'upload'
	);
	// Logo icon
	$options[] = array(
		'name' => __( 'Logo icon'),
		'desc' => __( 'Upload logo icon, appears next to the logo'),
		'id' => 'logo_icon',
		'type' => 'upload'
	);

	// Favicon image
	$options[] = array(
		'name' => __( 'Favicon image'),
		'desc' => __( 'We recommend a 512×512 pixel PNG image'),
		'id' => 'fav_icon',
		'type' => 'upload'
	);

	// top games nav toggle
	$options[] = array(
		'name' => __( 'Enable / Disable Top Games Nav'),
		'desc' => sprintf( __('You can choose to display / hide the Top Right Games Nav.') ),
		'id' => 'top_games_menu_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);

	// top games nav games config
	$options[] = array(
		'name' => __( 'Add Games Grid shortcode for Top Games Nav'),
		'desc' => sprintf( __( 'You can control what games are displayed in the Top Right Games Nav. See shortcode instructions <a href="%1$s" target="_blank">here</a>'), 'https://vegashero.co/shortcode-to-display-games-from-individual-providers-operators-category-or-keyword/' ),
		'id' => 'top_games_menu_shortcode',
		'std' => '[vh-grid category="video slots" orderby="date" order="DESC" gamesperpage="5"]',
		'type' => 'text'
	);

	// toogle title banner strip globally
	$options[] = array(
		'name' => __( 'Show / Hide title banner strip on posts, pages, games and casino reviews'),
		'desc' => sprintf( __('You can choose to display the H2 additional title with a background strip above the post / page content') ),
		'id' => 'title_banner_strip_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);


	//************* HOMEPAGE SETTINGS *************//
	
	$options[] = array(
		'name' => __( 'Homepage Settings'),
		'type' => 'heading'
	);

	// Text for posts title homepage
	$options[] = array(
		'name' => __( 'Title for posts section'),
		'id' => 'homepage_posts_title_text',
		'std' => 'Latest news',
		'class' => 'mini',
		'type' => 'text'
	);

	// Home page width settings
	$options[] = array(
		'name' => __( 'Homepage width setup'),
		'desc' => __( 'Choose an option for the homepage.'),
		'id' => 'homepage_size',
		'std' => '70',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $layout_array
	);

	// Title for Archives page
	$options[] = array(
		'name' => __( 'Text title for archive page in top banner'),
		'id' => 'archive_title_text',
		'std' => 'All News',
		'class' => 'mini',
		'type' => 'text'
	);

	// Number of posts to display on homepage
	$options[] = array(
		'name' => __( 'Number of posts'),
		'id' => 'home_posts',
		'std' => '5',
		'class' => 'mini',
		'type' => 'text'
	);
	// Button for posts on homepage
	$options[] = array(
		'name' => __( 'Display "More news" button'),
		'desc' => __( 'This will override the option to display a specific category below'),
		'id' => 'display_all_posts_button_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);
	// Button text for posts button on homepage
	$options[] = array(
		'name' => __( 'Button text for "More news" button'),
		'id' => 'display_all_posts_button_text',
		'std' => 'More news',
		'class' => 'hidden',
		'type' => 'text'
	);

	if ( $options_categories ) {
		$options[] = array(
			'name' => __( 'Select a Category for the archive post page'),
			'desc' => __( 'This will display all posts for that category'),
			'id' => 'news_select_categories',
			'std' => '1',
			'type' => 'select',
			'options' => $options_categories
		);
	}

	// Button for games to show on homepage
	// $options[] = array(
	// 	'name' => __( 'Display "More games" button'),
	// 	'id' => 'display_all_games_button_checkbox',
	// 	'std' => '1',
	// 	'type' => 'checkbox'
	// );
	// Button text for games button on homepage
	// $options[] = array(
	// 	'name' => __( 'Button text for "More games" button'),
	// 	'id' => 'display_all_games_button_text',
	// 	'std' => 'More games',
	// 	'class' => 'mini',
	// 	'type' => 'text'
	// );

	// banner image
	$options[] = array(
		'name' => __( 'Home Bottom Banner'),
		'desc' => __( 'Banner area below the posts.'),
		'id' => 'banner_area',
		'type' => 'upload'
	);

	// banner link
	$options[] = array(
		'name' => __( 'Link for banner'),
		'id' => 'banner_area_link',
		'class' => 'mini',
		'type' => 'text'
	);

	//************* END HOMEPAGE SETTINGS *************//

	//************* BANNER SETTINGS *************//	

	$options[] = array(
		'name' => __( 'Banner Settings'),
		'type' => 'heading'
	);

	// enable / disable Main banner/slider
	$options[] = array(
		'name' => __( 'Enable / Disable Home page banner / slider'),
		'desc' => sprintf( __('You can choose to display / hide the Main banner or LayerSlider area on the home page.') ),
		'id' => 'enable_banner_slider',
		'std' => '1',
		'type' => 'checkbox'
	);

	// Slider shortcode
	$options[] = array(
		'name' => __( 'Add your LayerSlider shortcode (overrides Main Banner below)'),
		'desc' => sprintf( __( 'Create your slider here <a href="%1$s" target="_blank">here</a>'), 'admin.php?page=layerslider' ),
		'id' => 'layerslider',
		'std' => '',
		'type' => 'text'
	);

	// Homepage banner image
	$options[] = array(
		'name' => __( 'Main Static Banner'),
		'desc' => __( 'Banner to show on homepage if no LayerSlider is active'),
		'id' => 'homepage_banner',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Banner large title'),
		'id' => 'banner_large_title',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Banner description title'),
		'id' => 'banner_description_title',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Banner button title'),
		'id' => 'banner_button_title',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Banner button link'),
		'id' => 'banner_button_link',
		'std' => 'http://',
		'type' => 'text'
	);


	// $options[] = array(
	// 	'name' => __( 'Banner Textarea'),
	// 	'desc' => __( 'Banner Textarea.'),
	// 	'id' => 'homepage_banner_html',
	// 	'std' => 'Default Text',
	// 	'type' => 'textarea'
	// );


	//************* END BANNER SETTINGS *************//

	//************* SOCIAL NETWORKING SETTINGS *************//

	$options[] = array(
		'name' => __( 'Social Settings'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Facebook link'),
		'id' => 'facebook',
		'std' => 'https://www.facebook.com/VegasHeroPlugin/',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Twitter link'),
		'id' => 'twitter',
		'std' => 'http://twitter.com/Vegas_Hero',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Google link'),
		'id' => 'google',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Pinterest link'),
		'id' => 'pinterest',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Instagram link'),
		'id' => 'instagram',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Linkedin link'),
		'id' => 'linkedin',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Reddit link'),
		'id' => 'reddit',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Youtube link'),
		'id' => 'youtube',
		'std' => 'https://www.youtube.com/channel/UCyATnW9Owi6u9al237gpu6Q',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Tumblr link'),
		'id' => 'tumblr',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'RSS link'),
		'id' => 'rss',
		'std' => '',
		'type' => 'text'
	);

	//************* END SOCIAL NETWORKING SETTINGS *************//


	//************* GAME PAGE SETTINGS *************//

	$options[] = array(
		'name' => __( 'Game Page Settings'),
		'type' => 'heading'
	);
	$options[] = array(
		'name' => __( 'Enable Global Affiliate button (This will overwrite the mini table on every game post)'),
		'desc' => sprintf( __('This will add a large button link on the right sidebar of all game pages.') ),
		'id' => 'global_link_checkbox',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Global Affiliate button text'),
		'id' => 'global_affiliate_link_text',
		'std' => 'Play for real',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Global Affiliate button link'),
		'id' => 'global_affiliate_link',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Display Tablepress mini table (Only shows if Global Affiliate button is disabled)'),
		'desc' => sprintf( __('Show Mini dropdown table + button on the right sidebar of all game pages. Outputs TablePress shortcode content set below.') ),
		'id' => 'mini_table_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);
	$options[] = array(
		'name' => __( 'Mini dropdown table button text'),
		'desc' => sprintf( __('Large button with dropdown option displayed on the right sidebar of games. Dropdown table can be set below via shortcode (TablePress only).') ),
		'id' => 'play_now_text',
		'std' => 'Visit Casino',
		'type' => 'text'
	);
	$options[] = array(
		'name' => __( 'Title for TablePress table (displayed below games)'),
		'desc' => sprintf( __('This is displayed below the games (you can also use the VegasHero Single Games Widget Area for displaying tables under each game)') ),
		'id' => 'bonus_table_title',
		'std' => '',
		'type' => 'text'
	);
	$options[] = array(
		'name' => __( 'Add your TablePress shortcode to all game pages'),
		'desc' => sprintf( __( 'This is displayed below the games and on the right hand side in the Mini dropdown table as well if enabled. Configure your TablePress table <a href="%1$s" target="_blank">here</a>'), 'admin.php?page=tablepress' ),
		'id' => 'bonus_table',
		'std' => '',
		'type' => 'text'
	);
	$options[] = array(
		'name' => __( 'Enable/disable sticky mobile CTA button'),
		'desc' => sprintf( __('This will make the global affiliate link button and the mini-table dropdown button always display on the bottom of mobile screens') ),
		'id' => 'sticky_mobile_affiliate_link_checkbox',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Display review stars'),
		'desc' => sprintf( __('Display the star reviews above button (When WP-Reviews star ratings are configured)') ),
		'id' => 'display_review_stars_checkbox',
		'std' => '0',
		'type' => 'checkbox'
	);
	$options[] = array(
		'name' => __( 'Game excerpt title'),
		'desc' => sprintf( __('This is displayed on the right sidebar of games posts. The excerpt area will only show if the game post excerpt field is not empty.') ),
		'id' => 'game_excerpt_title',
		'std' => 'Game Details',
		'type' => 'text'
	);


	//************* END GAME PAGE SETTINGS *************//

	//************* CASINO PAGE SETTINGS *************//

	$options[] = array(
		'name' => __( 'Casino Page Settings'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Visit Casino button text'),
		'desc' => sprintf( __('Large button displayed on the right sidebar of the casino reviews post page') ),
		'id' => 'casino_title_button',
		'std' => 'Visit Casino',
		'type' => 'text'
	);
	$options[] = array(
		'name' => __( 'Open casino affiliate link in new browser window?'),
		'desc' => sprintf( __('If checked the Visit Casino button and the link on the top of the review panel will open in new window') ),
		'id' => 'casino_link_target_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Casino excerpt title'),
		'desc' => sprintf( __('This is displayed on the right sidebar of casino posts') ),
		'id' => 'casino_excerpt_title',
		'std' => 'Casino Details',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Number of casinos to display on casinos main page'),
		'desc' => sprintf( __('Number of casinos to display on the casinos main index page. We recommend the number to be multiples of 3 (example 3/6/9 etc) as the layout is set to 3 columns. (the rest will be paginated)') ),
		'id' => 'casino_posts',
		'std' => '6',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Casino post button text to display on casinos main page'),
		'desc' => sprintf( __('You can set the button text that displays on the casino post cards on the main casinos index page') ),
		'id' => 'casino_posts_btn_text',
		'std' => 'Get Bonus',
		'type' => 'text'
	);
	$options[] = array(
		'name' => __( 'Override the Casino post button to become your affiliate link'),
		'desc' => sprintf( __('If checked the Casino post button on the casino post cards will link your affiliate link directly and open the promote casino link in a new window. The rest of the casino card will still link to the casino review post.') ),
		'id' => 'casino_posts_btn_override',
		'std' => '0',
		'type' => 'checkbox'
	);
	$options[] = array(
		'name' => __( 'Display casino posts on top'),
		'desc' => sprintf( __('If checked the casino post cards will display on top of the index page and the page text will be below them') ),
		'id' => 'casino_posts_ontop',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Casino Custom Post Type URL slug'),
		'desc' => sprintf( __('You can set the path for the Casino post URLs here. Example www.yoursite.com/<b>casinos</b>/casino-name/. This CANNOT be the same as your main casinos index page URL!') ),
		'id' => 'casino_cpt_slug',
		'std' => 'casinos',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enable/disable sticky mobile CTA button'),
		'desc' => sprintf( __('This will make the casino link button always display on the bottom of mobile screens') ),
		'id' => 'sticky_mobile_casino_link_checkbox',
		'std' => '0',
		'type' => 'checkbox'
	);

	//************* END CASINO PAGE SETTINGS *************//

	//************* REVIEWS SECTION SETTINGS *************//

	$options[] = array(
		'name' => __( 'Review Styling Settings'),
		'type' => 'heading'
	);

	if ( $options_categories ) {
		$options[] = array(
			'name' => __( 'Select a Style for the review block'),
			'id' => 'review_style_select',
			'type' => 'select',
			'options' => $options_review_style
		);
	}

	//************* END REVIEWS SECTION SETTINGS *************//

	//************* Table builder **********************//

	// $options[] = array(
	// 	'name' => __( 'Table Builder'),
	// 	'type' => 'heading'
	// );


	// if ( $options_categories ) {
	// 	$options[] = array(
	// 		'name' => __( 'Select a Category'),
	// 		'desc' => __( 'Passed an array of categories with cat_ID and cat_name'),
	// 		'id' => 'example_select_categories',
	// 		'type' => 'select',
	// 		'options' => $options_categories
	// 	);
	// }
	//
	// $options[] = array(
	// 	'name' => __( 'Input Text'),
	// 	'desc' => __( 'A text input field.'),
	// 	'id' => 'example_text',
	// 	'std' => 'Default Value',
	// 	'type' => 'text'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Input with Placeholder'),
	// 	'desc' => __( 'A text input field with an HTML5 placeholder.'),
	// 	'id' => 'example_placeholder',
	// 	'placeholder' => 'Placeholder',
	// 	'type' => 'text'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Textarea'),
	// 	'desc' => __( 'Textarea description.'),
	// 	'id' => 'example_textarea',
	// 	'std' => 'Default Text',
	// 	'type' => 'textarea'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Input Select Small'),
	// 	'desc' => __( 'Small Select Box.'),
	// 	'id' => 'example_select',
	// 	'std' => 'three',
	// 	'type' => 'select',
	// 	'class' => 'mini', //mini, tiny, small
	// 	'options' => $test_array
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Input Select Wide'),
	// 	'desc' => __( 'A wider select box.'),
	// 	'id' => 'example_select_wide',
	// 	'std' => 'two',
	// 	'type' => 'select',
	// 	'options' => $test_array
	// );
	//
	//
	//
	// $options[] = array(
	// 	'name' => __( 'Select a Page'),
	// 	'desc' => __( 'Passed an pages with ID and post_title'),
	// 	'id' => 'example_select_pages',
	// 	'type' => 'select',
	// 	'options' => $options_pages
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Input Radio (one)'),
	// 	'desc' => __( 'Radio select with default options "one".'),
	// 	'id' => 'example_radio',
	// 	'std' => 'one',
	// 	'type' => 'radio',
	// 	'options' => $test_array
	// );


	//
	// $options[] = array(
	// 	'name' => __( 'Hidden Text Input'),
	// 	'desc' => __( 'This option is hidden unless activated by a checkbox click.'),
	// 	'id' => 'example_text_hidden',
	// 	'std' => 'Hello',
	// 	'class' => 'hidden',
	// 	'type' => 'text'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Uploader Test'),
	// 	'desc' => __( 'This creates a full size uploader that previews the image.'),
	// 	'id' => 'example_uploader',
	// 	'type' => 'upload'
	// );
	//

	//
	// $options[] = array(
	// 	'name' =>  __( 'Example Background'),
	// 	'desc' => __( 'Change the background CSS.'),
	// 	'id' => 'example_background',
	// 	'std' => $background_defaults,
	// 	'type' => 'background'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Multicheck'),
	// 	'desc' => __( 'Multicheck description.'),
	// 	'id' => 'example_multicheck',
	// 	'std' => $multicheck_defaults, // These items get checked by default
	// 	'type' => 'multicheck',
	// 	'options' => $multicheck_array
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Colorpicker'),
	// 	'desc' => __( 'No color selected by default.'),
	// 	'id' => 'example_colorpicker',
	// 	'std' => '',
	// 	'type' => 'color'
	// );
	//
	// $options[] = array( 'name' => __( 'Typography'),
	// 	'desc' => __( 'Example typography.'),
	// 	'id' => "example_typography",
	// 	'std' => $typography_defaults,
	// 	'type' => 'typography'
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Custom Typography'),
	// 	'desc' => __( 'Custom typography options.'),
	// 	'id' => "custom_typography",
	// 	'std' => $typography_defaults,
	// 	'type' => 'typography',
	// 	'options' => $typography_options
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Text Editor'),
	// 	'type' => 'heading'
	// );

	/**
	 * For $settings options see:
	 * http://codex.wordpress.org/Function_Reference/wp_editor
	 *
	 * 'media_buttons' are not supported as there is no post to attach items to
	 * 'textarea_name' is set by the 'id' you choose
	 */

	// $wp_editor_settings = array(
	// 	'wpautop' => true, // Default
	// 	'textarea_rows' => 5,
	// 	'tinymce' => array( 'plugins' => 'wordpress,wplink' )
	// );
	//
	// $options[] = array(
	// 	'name' => __( 'Default Text Editor'),
	// 	'desc' => sprintf( __( 'You can also pass settings to the editor.  Read more about wp_editor in <a href="%1$s" target="_blank">the WordPress codex</a>'), 'http://codex.wordpress.org/Function_Reference/wp_editor' ),
	// 	'id' => 'example_editor',
	// 	'type' => 'editor',
	// 	'settings' => $wp_editor_settings
	// );

	return $options;
}
