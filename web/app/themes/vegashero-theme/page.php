<?php get_header(); ?>

<?php if (of_get_option('title_banner_strip_checkbox', '1')) { ?>

<div class="game-post-banner">
	<h2><?php if (get_field('field_title_banner_strip')) { echo get_field('field_title_banner_strip'); } else { the_title(); } ?></h2>
	<div class="image" style="background: url('<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $image_url; ?>') no-repeat; background-size:cover;">
	</div>
</div>

<?php } ?>

	<div id="content">

		<div id="inner-content" class="row">

		    <main id="main" class="large-8 medium-8 columns" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>

			    <?php endwhile; endif; ?>

			</main> <!-- end #main -->
			<div id="sidebar1" class="sidebar-wrapper large-4 medium-12 columns divider" role="complementary">
				<?php get_sidebar(); ?>
			</div>
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
